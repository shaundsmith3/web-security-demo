package com.shaundsmith.exception;

/**
 * Created by Shaun on 28/09/2016.
 */
public class InvalidLoginCredentialsException extends Exception {

    public InvalidLoginCredentialsException(String message) {
        super(message);
    }
}
