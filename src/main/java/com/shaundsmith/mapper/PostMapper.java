package com.shaundsmith.mapper;

import com.shaundsmith.entity.Post;
import com.shaundsmith.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Shaun on 18/09/2016.
 */
public class PostMapper implements RowMapper<Post> {

    public Post mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
        Post post = new Post();
        User user = new User();
        user.setUserId(resultSet.getInt("user_id"));
        user.setUsername(resultSet.getString("username"));
        user.setPicture(resultSet.getString("picture"));
        post.setUser(user);
        post.setPostId(resultSet.getInt("post_id"));
        post.setTitle(resultSet.getString("post_title"));
        post.setContent(resultSet.getString("post_content"));

        return post;
    }

}
