package com.shaundsmith.mapper;

import com.shaundsmith.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Shaun on 18/09/2016.
 */
public class UserMapper implements RowMapper<User> {

    public User mapRow(ResultSet resultSet,int rowNumber) throws SQLException {
        User user = new User();
        user.setUserId(resultSet.getInt("user_id"));
        user.setUsername(resultSet.getString("username"));
        user.setPassword(resultSet.getString("password"));
        user.setPicture(resultSet.getString("picture"));

        return user;
    }
}
