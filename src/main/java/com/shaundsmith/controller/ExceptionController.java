package com.shaundsmith.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@Controller
public class ExceptionController {

    @RequestMapping("error404")
    public ModelAndView notFoundError(HttpServletRequest request) throws UnsupportedEncodingException {
        ModelAndView mav = new ModelAndView("error404");
        String originalUrl = (String)request.getAttribute("javax.servlet.forward.request_uri");
        mav.addObject("requestedPage", URLDecoder.decode(originalUrl, "UTF-8"));
        return mav;
    }
}
