package com.shaundsmith.controller;

import com.shaundsmith.dao.PostDaoImpl;
import com.shaundsmith.dao.UserDaoImpl;
import com.shaundsmith.entity.Post;
import com.shaundsmith.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/")
public class PostController {

    private static final String POSTS_PARAM = "posts";
    private static final String PAGE_TITLE_PARAM = "pageTitle";

    @Autowired
    private PostDaoImpl postDao;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView mav = new ModelAndView("index");
        List<Post> posts = postDao.getRecentPosts();
        mav.addObject(POSTS_PARAM, posts);
        mav.addObject(PAGE_TITLE_PARAM, "Recent Posts");
        mav.addObject("isHome", true);
        return mav;
    }

    @RequestMapping(value = "post", method = RequestMethod.GET)
    public ModelAndView viewPost(Integer id) {
        Post post;
        try {
             post = postDao.findById(id);
        } catch (Exception e) {
            throw new NotFoundException("Post with ID " + id + " not found");
        }

        ModelAndView mav = new ModelAndView("index");
        mav.addObject(POSTS_PARAM, Collections.singletonList(post));
        mav.addObject(PAGE_TITLE_PARAM, "Viewing post");

        return mav;
    }

    @RequestMapping(value = "userPosts", method = RequestMethod.GET)
    public ModelAndView viewUsersPosts(@RequestParam("id") Integer id) {
        List<Post> posts = Collections.emptyList();
        if (id != null) {
            posts = postDao.findByUserId(id);
        }

        ModelAndView mav = new ModelAndView("index");
        mav.addObject(POSTS_PARAM, posts);
        mav.addObject(PAGE_TITLE_PARAM, "Posts By " + (posts.isEmpty() ? "user" : posts.get(0).getUser().getUsername()));

        return mav;
    }

    @RequestMapping(value = "createPost", method = RequestMethod.POST)
    public void createPost(@RequestParam("userId") Integer userId,
            @RequestParam("title") String title,
            @RequestParam("content") String content,
            HttpServletResponse response) throws IOException {
        Post post = transform(userId, title, content);

        List<String> errors = validate(post);
        if (errors.isEmpty()) {
            Integer newId = postDao.create(post);
            response.sendRedirect("post?id=" + newId);
        } else {
            response.sendRedirect("?id=Error%20creating%20post");
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public void deletePost(HttpServletRequest request, HttpServletResponse response, @RequestParam("id")Integer id) throws IOException {
        Post post = postDao.findById(id);
        User currentUser = (User) request.getSession().getAttribute("user");
        if (currentUser.getUserId().equals(post.getUser().getUserId())) {
            postDao.delete(id);
        }
        response.sendRedirect("/");
    }

    private Post transform(Integer userId, String title, String content) {
        Post post = new Post();
        HtmlEscaper htmlEscaper = new HtmlEscaper();
        User user = new User();
        user.setUserId(userId);
        post.setContent(htmlEscaper.escapeHtml(content));
        post.setUser(user);
        post.setTitle(htmlEscaper.escapeHtml(title));

        return post;
    }

    private List<String> validate(Post post) {
        List<String> errors = new ArrayList<>();
        if (post.getUser() == null || post.getUser().getUserId() == null || post.getUser().getUserId() < 1) {
            errors.add("userId");
        }
        if (post.getTitle() == null || post.getTitle().length() > 50) {
            errors.add("title");
        }
        if (post.getContent() == null || post.getContent().length() > 255) {
            errors.add("content");
        }

        return errors;
    }


}
