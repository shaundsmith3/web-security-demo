package com.shaundsmith.controller;

import com.google.gson.Gson;
import com.shaundsmith.dao.PostDaoImpl;
import com.shaundsmith.dao.UserDaoImpl;
import com.shaundsmith.entity.Post;
import com.shaundsmith.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    private MessageDigest md5;

    private Gson gson;

    @Autowired
    private UserDaoImpl userDao;

    public UserController() throws NoSuchAlgorithmException {
        md5 = MessageDigest.getInstance("MD5");
        gson = new Gson();
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public void login(@RequestParam("username") String username,
                      @RequestParam("password") String password,
                      HttpServletRequest request,
                      HttpServletResponse response) throws IOException {
        String hashedPassword = hashPassword(password);
        try {
            User user = userDao.verifyUser(username, hashedPassword);
            request.getSession().setAttribute("user", user);
            response.sendRedirect("/");
        } catch (Exception e) {
            response.sendRedirect("/?error=Login%20Error");
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().setAttribute("user", null);
        response.sendRedirect("/");
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView register(@RequestParam("username") String username,
                         @RequestParam("password") String password,
                         @RequestParam("picture") String picture,
                         HttpServletResponse response,
                         HttpServletRequest request) throws Exception {
        List<String> errors = validate(username, password);

        ModelAndView mav = new ModelAndView("index");
        if (errors.isEmpty()) {
            User user = new User();
            user.setPassword(hashPassword(password));
            user.setUsername(username);
            user.setPicture(picture);

            Integer newUserId = userDao.create(user);
            User newUser = userDao.verifyUser(username, hashPassword(password));
            request.getSession().setAttribute("user", newUser);
            response.sendRedirect("/");

        } else {
            response.sendRedirect("/?error=Errors%20with%20the%20following%20fields%20" + errors.stream().reduce("", (x, y) -> x + "%20" + y));
        }

        return mav;
    }

    @RequestMapping(value = "/getUserList", method = RequestMethod.GET)
    public void getUserList(HttpServletResponse response) throws IOException {
        List<User> users = userDao.findAll();
        String json = gson.toJson(users);

        response.setStatus(200);
        response.setContentType("application/json");
        response.getWriter().write(json);
        response.getWriter().flush();
        response.getWriter().close();
    }

    public String hashPassword(String password) {
        md5.update(password.getBytes());
        byte[] digest = md5.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }

    private List<String> validate(String username, String password) {
        List<String> errors = new ArrayList<>();
        if (username == null || username.length() > 50 || password.length() == 0) {
            errors.add("username");
        }
        if (password == null || password.length() == 0) {
            errors.add("password");
        }
        return errors;
    }

}
