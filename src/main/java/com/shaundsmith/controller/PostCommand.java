package com.shaundsmith.controller;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Shaun on 18/09/2016.
 */
public class PostCommand {

    @Autowired
    private HtmlEscaper htmlEscaper;

    private Integer postId;
    private Integer userId;
    private String title;
    private String content;

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = htmlEscaper.escapeHtml(title);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = htmlEscaper.escapeHtml(content);
    }
}
