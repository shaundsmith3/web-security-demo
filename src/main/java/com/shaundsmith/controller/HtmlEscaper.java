package com.shaundsmith.controller;

import java.util.Arrays;

public class HtmlEscaper {

    private static final String[] BANNED_HTML = new String[] {
        "html", "body", "h1", "h2", "h3", "h4", "h5", "h6", "title", "head", "p",
            "br", "hr", "acronym", "address", "bdi", "bdo", "big", "blockquote", "cite", "code",
            "del", "ins", "kbd", "mark", "meter", "pre", "progress", "q", "time", "form", "input",
            "textarea", "button", "select", "optgroup", "option", "label", "fieldset", "legend",
            "datalist", "keygen", "output", "iframe", "img", "map", "area", "canvas", "figcaption",
            "figure", "audio", "source", "track", "video", "a", "link", "nav", "ul", "li", "ol", "dl",
            "dt", "dd", "menu", "menuitem", "table", "caption", "th", "td", "tr", "tbody", "tfoot", "col",
            "colgroup", "style", "div", "span", "header", "footer", "main", "section", "article", "aside",
            "details", "dialog", "summary", "meta", "base", "script", "noscript", "object", "embed", "param"
    };

    private static final String HTML_REGEX = "<\\/?%s\\s*.*?>";

    public String escapeHtml(final String input) {
        return Arrays.stream(BANNED_HTML)
                .reduce(input, (x, y) -> x.replaceAll(String.format(HTML_REGEX, y), "&lt;" + y + "&gt;"));
    }
}
