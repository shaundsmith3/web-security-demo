package com.shaundsmith.entity;

public class User {

    private Integer userId;
    private String username;
    private String password;
    private String picture;

    public User() {
        //EMPTY
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean equals(User user) {
        return getUserId() == user.getUserId();
    }
}
