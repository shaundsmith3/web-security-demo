package com.shaundsmith.entity;

/**
 * Created by Shaun on 18/09/2016.
 */
public class Post {

    private Integer postId;
    private User user;
    private String title;
    private String content;

    public Post() {
        //EMPTY
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
