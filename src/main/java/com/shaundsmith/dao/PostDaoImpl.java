package com.shaundsmith.dao;

import com.shaundsmith.entity.Post;
import com.shaundsmith.mapper.PostMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public final class PostDaoImpl implements PostDao, CrudDao<Post> {

    private static final String COMMON_SELECT_QUERY = "SELECT * FROM posts " +
            "JOIN users ON posts.user_id = users.user_id ";

    private static final String SELECT_RECENT_QUERY = COMMON_SELECT_QUERY +
            "ORDER BY post_id DESC LIMIT 25;";

    private static final String SELECT_QUERY = COMMON_SELECT_QUERY +
            "WHERE post_id = ?;";

    private static final String SELECT_USER_ID_QUERY = COMMON_SELECT_QUERY +
            "WHERE users.user_id = ? ORDER BY post_id;";

    private static final String CREATE_QUERY = "INSERT INTO posts (user_id, post_title, post_content) VALUES (?, ?, ?)";

    private static final String DELETE_QUERY = "DELETE FROM posts WHERE post_id = ?";

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public Integer create(final Post post) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        PreparedStatementCreator preparedStatementCreator = (connection) -> {
                PreparedStatement preparedStatement = connection.prepareStatement(CREATE_QUERY, new String[]{"post_id"});
                preparedStatement.setInt(1, post.getUser().getUserId());
                preparedStatement.setString(2, post.getTitle());
                preparedStatement.setString(3, post.getContent());

                return preparedStatement;
        };
        jdbcTemplate.update(preparedStatementCreator, keyHolder);
        return (Integer)keyHolder.getKey();
    }

    public void delete(Integer id) {
        jdbcTemplate.update(DELETE_QUERY, new Object[]{id});
    }

    public Post findById(Integer id) {
        Post post = jdbcTemplate.queryForObject(SELECT_QUERY, new Object[]{id}, new PostMapper());
        return post;
    }

    public List<Post> getRecentPosts() {
        List<Post> posts = jdbcTemplate.query(SELECT_RECENT_QUERY, new PostMapper());
        return posts;
    }

    public List<Post> findByUserId(Integer userId) {
        List<Post> posts = jdbcTemplate.query(SELECT_USER_ID_QUERY, new Object[]{userId}, new PostMapper());
        return posts;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

}
