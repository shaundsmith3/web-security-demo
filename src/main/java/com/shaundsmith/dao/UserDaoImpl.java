package com.shaundsmith.dao;

import com.shaundsmith.entity.User;
import com.shaundsmith.exception.InvalidLoginCredentialsException;
import com.shaundsmith.mapper.UserMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public final class UserDaoImpl implements UserDao, CrudDao<User> {

    private static final String LOGIN_QUERY = "SELECT * FROM users WHERE username = ? AND password = ?;";

    private static final String REGISTER_QUERY = "INSERT INTO users (username, password, picture) VALUES (?, ?, ?);";

    private static final String FIND_ALL_QUERY = "SELECT * FROM users;";

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public Integer create(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        PreparedStatementCreator preparedStatementCreator = (connection) -> {
            PreparedStatement preparedStatement = connection.prepareStatement(REGISTER_QUERY, new String[]{"user_id"});
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getPicture());

            return preparedStatement;
        };

        jdbcTemplate.update(preparedStatementCreator, keyHolder);
        return (Integer)keyHolder.getKey();
    }

    public void delete(Integer id) {
        throw new UnsupportedOperationException("You cannot delete a user");
    }

    public User findById(Integer id) {
        return null;
    }

    public List<User> findAll() {
        return jdbcTemplate.query(FIND_ALL_QUERY, new UserMapper());
    }

    public User verifyUser(String username, String password) throws InvalidLoginCredentialsException {
        List<User> usersFound = jdbcTemplate.query(LOGIN_QUERY, new Object[]{username, password}, new UserMapper());
        if (usersFound.isEmpty()) {
            throw new InvalidLoginCredentialsException("User '" + username + "' failed to login");
        }
        return usersFound.get(0);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


}
