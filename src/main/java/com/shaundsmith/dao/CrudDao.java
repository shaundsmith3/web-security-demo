package com.shaundsmith.dao;

public interface CrudDao<R> {

    Integer create(R entity);

    void delete(Integer id);

    R findById(Integer id);

}
