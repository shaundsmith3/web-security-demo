package com.shaundsmith.dao;

import com.shaundsmith.entity.User;
import com.shaundsmith.exception.InvalidLoginCredentialsException;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Shaun on 18/09/2016.
 */
public interface UserDao {

    User verifyUser(String username, String password) throws InvalidLoginCredentialsException;

    List<User> findAll();

    void setDataSource(DataSource dataSource);

}
