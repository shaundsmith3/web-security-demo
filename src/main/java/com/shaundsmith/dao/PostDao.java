package com.shaundsmith.dao;

import com.shaundsmith.entity.Post;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Shaun on 18/09/2016.
 */
public interface PostDao {

    void setDataSource(DataSource dataSource);

    List<Post> getRecentPosts();
}
