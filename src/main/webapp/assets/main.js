$(document).ready(function() {
    //If the current users div is present, fetch the current users
    $('#current-users').length && fetchCurrentUsers();

    $('#login-link').on('click', function() {
        $('#login-form').show();
    });

    $('#signup-link').on('click', function() {
        $('#signup-form').show();
    });

    $('#new-post-link').on('click', function() {
       $('#new-post-form').show();
    });

    $('.popup').on('click', function() {
        $(this).hide();
    });

    $('.popup-form').on('click', function(e) {
        e.stopPropagation();
    });
});

//Replace Broken images with the colour logo image
function replaceBrokenImage(image) {
    image.src = "assets/logo.png";
    image.onerror = "";
}

//Fetch the current users list via an AJAX call
function fetchCurrentUsers() {
    var currentUserContainer = $('#current-users');
    $.ajax({
        method: "get",
        url: "user/getUserList"
    }).done(function(data) {
        var html = "";
        for (var i = 0; i < data.length; i++) {
            html += renderUser(data[i]);
        }
        currentUserContainer.html(html);
    });

    setTimeout(fetchCurrentUsers, 60000);
}

//Render the user
function renderUser(user) {
    return  '<a href="/userPosts?id=' + user.userId +'">' +
                '<img src="' + user.picture + '" width="50" height="50" class="user-picture" border="1" onerror="replaceBrokenImage(this);" />' +
            '</a>';
}