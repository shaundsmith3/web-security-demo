<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Post Layout" pageEncoding="UTF-8" %>
<%@attribute name="username"%>
<%@attribute name="title" %>
<%@attribute name="content" %>
<%@attribute name="id" %>
<div class="post">
    <div class="post-title">
        <a href="post?id=${id}">
            <c:out value="${title}" escapeXml="false" />
        </a>
    </div>
    <div class="post-user"><c:out value="${username}" /></div>
    <div class="post-content"><c:out value="${content}" escapeXml="false" /></div>
    <c:if test="${sessionScope.user != null}">
        <c:if test="${sessionScope.user.username == username}">
            <div class="delete-button">
            	<a href="delete?id=<c:out value="${id}" />">Delete</a>
            </div>
        </c:if>
    </c:if>
</div>