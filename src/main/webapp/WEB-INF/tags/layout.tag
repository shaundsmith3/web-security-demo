<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@tag description="Page Layout" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow" rel="stylesheet">
    <link href="assets/main.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="assets/main.js"></script>
    <title>Chirper</title>
</head>
<body>
<header>
    <img src="assets/silhouette-logo.png" width="50" height="50" id="logo" />
    <nav>
        <ul id="left-nav">
            <li><a href="/">Home</a></li>
            <c:if test="${sessionScope.user != null}">
                <li><a href="javascript:void;" id="new-post-link">Create Post</a></li>
            </c:if>
        </ul>
        <ul id="right-nav">
            <c:choose>
                <c:when test="${sessionScope.user != null}">
                    <li>Welcome <c:out value="${sessionScope.user.username}" /></li>
                    <li><a href="user/logout">Log out</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="javascript:void;" id="login-link">Log In</a></li>
                    <li><a href="javascript:void;" id="signup-link">Sign Up</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </nav>
</header>
<div id="wrapper">
    <div id="left-bar">
        <h2>Current Users</h2>
        <div id="current-users"></div>
    </div>
    <div id="right-wrapper">
        <div id="body-wrapper">
            <c:if test="${pageTitle != null}">
                <h2>
                    <c:out value="${pageTitle}" />
                </h2>
            </c:if>
            <jsp:doBody />
        </div>
    </div>
</div>
<t:popups />
</body>
</html>