<%@tag description="Popups" pageEncoding="UTF-8" %>
<div id="login-form" class="popup">
    <form method="get" action="user/login" class="popup-form">
        <label for="username">Username:</label>
        <input type="text" id="username" name="username"/>
        <br />
        <label for="password">Password:</label>
        <input type="password" id="password" name="password"/>
        <br />
        <input type="submit" value="Log In" />
    </form>
</div>
<div id="signup-form" class="popup">
    <form method="post" action="user/register" class="popup-form">
        <label for="username-signup">Username:</label>
        <input type="text" id="username-signup" name="username" />
        <br />
        <label for="password-signup">Password:</label>
        <input type="password" id="password-signup" name="password"/>
        <br />
        <label for="picture">Profile Picture:</label>
        <input type="text" id="picture" name="picture"/>
        <br />
        <input type="submit" value="Log In" />
    </form>
</div>
<div id="new-post-form" class="popup">
    <form method="post" action="createPost" class="popup-form">
        <input type="hidden" id="userId" name="userId" value="${sessionScope.user.userId}" />
        <label for="title">Title:</label>
        <input type="text" id="title" name="title" />
        <br />
        <label for="content">Content:</label>
        <textarea id="content" name="content"></textarea>
        <br />
        <input type="submit" value="Post" />
    </form>
</div>