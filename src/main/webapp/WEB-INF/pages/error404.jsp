<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>

<t:layout>
    <jsp:body>
        <h1>404 - Page Not Found</h1>
        <p>${requestedPage} not found on the server</p>
    </jsp:body>
</t:layout>