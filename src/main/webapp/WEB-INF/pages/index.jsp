<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:layout>
    <jsp:body>
        <c:forEach items="${posts}" var="post">
            <t:post username="${post.user.username}" title="${post.title}" content="${post.content}" id="${post.postId}" />
        </c:forEach>
    </jsp:body>
</t:layout>